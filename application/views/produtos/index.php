<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="<?=base_url("css/bootstrap.css")?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url("css/style.css")?> " />
    <title>Produtos</title>
</head>
<body>
    <div class="container">
        <div class="col-xs-12 col-sm-12">
            <div class="row">
                <?php if($this->session->flashdata("success")) : ?>
                    <p class="alert alert-success"><?= $this->session->flashdata("success") ?></p>
                <?php endif ?>
                <?php if($this->session->flashdata("danger")) : ?>
                    <p class="alert alert-danger"><?= $this->session->flashdata("danger") ?></p>
                <?php endif ?>
            </div>
        </div>
        <?php if($this->session->userdata("usuario_autenticado")) : ?>
            <div class="col-xs-12 col-sm-12">
                <div class="row">
                    <?=anchor("Produtos/form", "Novo Produto", array("class" => "btn btn-primary"))?>
                    <?=anchor("Login/logout", "Sair", array("class" => "btn btn-primary"))?>
                </div>
            </div>
        <?php else : ?>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                    <div class="bs-callout bs-callout-primary">
                        <h4>Login</h4>

                        <?php
                        echo form_open("Login/autentica");

                        echo form_label("Email:", "email");
                        echo form_input(array (
                            "name" => "email",
                            "id" => "email",
                            "class" => "form-control",
                            "maxlength" => "255",
                            "placeholder" => "Seu email",
                            "type" => "email",
                            "required" => "true"
                        ));

                        echo form_label("Senha:", "senha");
                        echo form_input(array (
                            "name" => "senha",
                            "id" => "senha",
                            "class" => "form-control",
                            "maxlength" => "255",
                            "placeholder" => "Sua senha",
                            "type" => "password",
                            "required" => "true"
                        ));

                        echo form_button(array(
                            "class" => "btn btn-primary",
                            "content" => "Entrar",
                            "type" => "submit"
                        ));

                        echo form_close();

                        ?>
                    </div>
                </div>
            </div>

        <?php endif ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="bs-callout bs-callout-primary">
                    <h4>Produtos</h4>
                    <table class="table table-hover">
                        <thead>
                            <th>Nome</th>
                            <th>Descrição</th>
                            <th class="text-center">Preço</th>
                        </thead>
                        <tbody>
                            <?php foreach ($produtos as $produto) : ?>
                                <tr>
                                    <td><?=anchor("Produtos/{$produto['id']}", $produto["nome"])?></td>
                                    <td><?=character_limiter(html_escape($produto["descricao"]), 10)?></td>
                                    <td class="text-right"><?=numeroEmReais($produto["preco"])?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="bs-callout bs-callout-primary">
                    <h4>Cadastro de Usuários</h4>

                    <?php

                    echo form_open("usuarios/novo");

                    echo form_label("Nome:", "nome");
                    echo form_input(array (
                        "name" => "nome",
                        "id" => "nome",
                        "class" => "form-control",
                        "maxlength" => "255",
                        "placeholder" => "Seu nome",
                        "type" => "text",
                        "required" => "true"
                    ));

                    echo form_label("Email:", "email");
                    echo form_input(array (
                        "name" => "email",
                        "id" => "email",
                        "class" => "form-control",
                        "maxlength" => "255",
                        "placeholder" => "Seu email",
                        "required" => "true",
                        "type" => "email"
                    ));

                    echo form_label("Senha:", "senha");
                    echo form_input(array (
                        "name" => "senha",
                        "id" => "senha",
                        "class" => "form-control",
                        "maxlength" => "255",
                        "placeholder" => "Sua senha",
                        "type" => "password",
                        "required" => "true"

                    ));

                    echo form_button(array(
                        "class" => "btn btn-primary",
                        "content" => "Salvar",
                        "type" => "submit"
                    ));

                    echo form_close();

                    ?>

                </div>
            </div>
        </div>
    </div>

</body>
</html>
