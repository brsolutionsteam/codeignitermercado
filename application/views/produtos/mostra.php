<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="<?=base_url("css/bootstrap.css")?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url("css/style.css")?> " />
    <title>Produto</title>
</head>
<body>
    <div class="container">
        <div class="col-xs-12 col-sm-12">
            <div class="row">
                <div class="bs-callout bs-callout-primary">
                    <h4>Produto: <?=$produto["nome"]?></h4>
                    Preço: <?=numeroEmReais($produto["preco"])?><br />
                    Descrição: <?=auto_typography(html_escape($produto["descricao"]))?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
