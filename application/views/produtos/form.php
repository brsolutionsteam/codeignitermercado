<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="<?=base_url("css/bootstrap.css")?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url("css/style.css")?> " />
    <title>Produtos</title>
</head>
<body>
    <div class="container">
        <div class="col-xs-12 col-sm-12">
            <div class="row">
                <div class="bs-callout bs-callout-primary">
                    <h4>Cadastro de Produto</h4>
                    <?php
                    // echo validation_errors();
                    echo form_open("Produtos/novo");

                    echo form_label("Nome:", "nome");
                    echo form_input(array (
                        "name" => "nome",
                        "id" => "nome",
                        "class" => "form-control",
                        "maxlength" => "255",
                        "placeholder" => "Nome do Produto",
                        "type" => "text",
                        "value" => set_value("nome", "")
                    ));
                    echo form_error("nome");

                    echo form_label("Preço:", "preco");
                    echo form_input(array (
                        "name" => "preco",
                        "id" => "preco",
                        "class" => "form-control",
                        "maxlength" => "255",
                        "placeholder" => "Preço do Produto",
                        "type" => "number",
                        "value" => set_value("preco", "")
                    ));
                    echo form_error("preco");

                    echo form_label("Descrição:", "descricao");
                    echo form_textarea(array(
                        "name" => "descricao",
                        "class" => "form-control",
                        "id" => "descricao",
                        "placeholder" => "Descrição do Produto",
                        "value" => set_value("descricao", "")
                    ));
                    echo form_error("descricao");

                    echo form_button(array(
                        "class" => "btn btn-primary",
                        "content" => "Salvar",
                        "type" => "submit"
                    ));

                    echo form_close();
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
