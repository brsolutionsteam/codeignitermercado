<?php
    class Usuarios_model extends CI_Model {

        public function salva($usuario) {
            $this->db->insert("usuario", $usuario);
        }

        public function buscaPorEmailESenha($email, $senha) {
            // $this->db->where("email", $email);
            // $this->db->where("senha", $senha);
            // $usuario = $this->db->get("usuario")->row_array();
            // return $usuario;
            return $this->db->get_where("usuario", array("email" => $email, "senha" => $senha))->row_array();
        }

    }
 ?>
