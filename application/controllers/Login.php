<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    public function autentica() {
        $this->load->model("Usuarios_model");
        $email = $this->input->post("email");
        $senha = md5($this->input->post("senha"));
        $usuario = $this->Usuarios_model->buscaPorEmailESenha($email, $senha);
        // var_dump($usuario);
        if($usuario) {
            $this->session->set_userdata("usuario_autenticado", $usuario);
            // $dados = array("mensagem" => "Logado com sucesso");
            $this->session->set_flashdata("success", "Logado com sucesso");
        } else {
            // $dados = array("mensagem" => "Usuário e/ou senha inválidos");
            $this->session->set_flashdata("danger", "Usuário e/ou senha inválidos!");
        }

        redirect("/");
        // $this->load->view("login/autentica", $dados);
    }

    public function logout() {
        $this->session->unset_userdata("usuario_autenticado");
        $this->session->set_flashdata("success","Você saiu com sucesso");
        redirect("/");
    }
}

?>
