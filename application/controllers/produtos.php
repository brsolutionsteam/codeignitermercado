<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends CI_Controller {

    /**
    *
    */

    public function index() {
        // $this->output->enable_profiler(TRUE);
        // $this->load->database();
        // $this->load->helper("text");
        $this->load->model("Produtos_model");
        $produtos = $this->Produtos_model->buscaTodos();
        $dados = array("produtos" => $produtos);

        $this->load->helper(array("currency"));
        // $this->load->helper(array("currency"));

        // $this->load->helper(array("form"));
        // $this->load->template("produtos/index.php", $dados);

        $this->load->view("produtos/index.php", $dados);

    }

    public function form() {
        $this->load->view("produtos/form");
    }

    public function novo() {
        $this->form_validation->set_error_delimiters("<p class='alert alert-danger', </p>");
        $this->form_validation->set_rules("nome", "nome", "trim|required|min_length[2]|max_length[255]|callback_nao_tenha_a_palavra_melhor");
        $this->form_validation->set_rules("descricao", "descricao", "trim|required|min_length[10]");
        $this->form_validation->set_rules("preco", "preco", "required");

        if($this->form_validation->run() == TRUE) {
            $usuario_autenticado = $this->session->userdata("usuario_autenticado");
            $produto = array(
                "nome" => $this->input->post("nome"),
                "preco" => $this->input->post("preco"),
                "descricao" => $this->input->post("descricao"),
                "usuario_id" => $usuario_autenticado["id"]
            );
            $this->load->model("Produtos_model");
            $this->Produtos_model->salva($produto);
            $this->session->set_flashdata("success", "Produto salvo com sucesso");
            redirect("/");
        } else {
            $this->load->view("Produtos/form");
        }
    }

    public function mostra($id) {
        $this->load->helper(array("currency", "typography"));
        // $id = $this->input->get("id");
        $this->load->model("Produtos_model");
        $produto = $this->Produtos_model->busca($id);
        $dados = array("produto" => $produto);
        $this->load->view("produtos/mostra", $dados);
    }

    public function nao_tenha_a_palavra_melhor($nome) {
        if(strpos($nome, "melhor") !== FALSE) {
            $this->form_validation->set_message("nao_tenha_a_palavra_melhor", "O campo '%s' não pode conter a palavra 'melhor'");
            return FALSE;
        } else {
            return TRUE;
        }
    }
}

?>
